//! Basic hello world example.

extern crate ggez;
extern crate rand;
extern crate specs;
#[macro_use]
extern crate specs_derive;

use ggez::conf;
use ggez::conf::WindowMode;
use ggez::conf::FullscreenType;
use ggez::event;
use ggez::{Context, GameResult};
use ggez::graphics;
use std::env;
use std::path;
use rand::Rng;
use std::collections::HashMap;
use std::collections::hash_map::Entry;
use ggez::timer;
use specs::{Component, VecStorage, World, System, ReadStorage, WriteStorage, RunNow, Read, DispatcherBuilder, Dispatcher, Entities, LazyUpdate};
use ggez::graphics::DrawMode::Fill;


// First we make a structure to contain the game's state
struct MainState {
	frames: usize,
	//     ship_designs: Vec<ships::ShipDesign>,
	world: World,
	dispatcher: Dispatcher<'static, 'static>,
	texture_map: HashMap<String, graphics::Image>,
}

#[derive(Component, Debug)]
#[storage(VecStorage)]
struct Position {
	point: graphics::Point2,
}

#[derive(Component, Debug)]
#[storage(VecStorage)]
struct Texture {
	texture: graphics::Image,
}

#[derive(Component, Debug)]
#[storage(VecStorage)]
struct Scale(graphics::Point2);

#[derive(Component, Debug)]
#[storage(VecStorage)]
struct Alpha(f64);

#[derive(Component, Debug)]
#[storage(VecStorage)]
struct Speed(graphics::Point2);

#[derive(Default)]
struct DeltaTime(u64);

#[derive(Default)]
struct ScreenResolution(graphics::Rect);

#[derive(Default)]
struct Imgs(Vec<graphics::Image>);

fn ndt(dt: &DeltaTime) -> f32 {
	dt.0 as f32 / 1e9 * 60.0
}


struct Mover;
impl<'a> System<'a> for Mover {
	type SystemData = (
		WriteStorage<'a, Position>,
		Read<'a, DeltaTime>,
		ReadStorage<'a, Speed>,
	);
	
	fn run(&mut self, data: Self::SystemData) {
		use specs::Join;
		let (mut position, delta, speed) = data;
		let delta = ndt(&delta);
		for (mut position, speed) in (&mut position, &speed).join() {
			position.point.x -= speed.0.x * delta;
			position.point.y -= speed.0.y * delta;
		}
	}
}

struct Reaper;
impl<'a> System<'a> for Reaper {
	type SystemData = (
		ReadStorage<'a, Position>,
		Entities<'a>,
		Read<'a, ScreenResolution>,
		ReadStorage<'a, Scale>,
	);
	
	fn run(&mut self, data: Self::SystemData) {
		use specs::Join;
		let (positions, entities, screen_res, scale) = data;
		
		for (pos, scale, mut ent) in (&positions, &scale, &*entities).join() {
			let sz = scale.0.y * 500.;
			if pos.point.x > screen_res.0.w || pos.point.y > screen_res.0.h || pos.point.x + sz < 0. || pos.point.y + sz < 0. {
				entities.delete(ent).expect("Why would this fail again?")
			}
		}
	}
}

struct Birther;
impl<'a> System<'a> for Birther {
	type SystemData = (
		Entities<'a>,
		Read<'a, ScreenResolution>,
		Read<'a, LazyUpdate>,
		Read<'a, Imgs>,
	);
	
	fn run(&mut self, data: Self::SystemData) {
		use specs::Join;
		let (entities, screen_res, updater, imgs) = data;
		
		let mut l :i64 = 0;
		for mut ent in (&*entities).join() {
			l += 1;
		}
		
		if l < 30 {
			let symbol = entities.create();
			let mut rng = rand::thread_rng();
			let vertical = rng.gen_bool(0.5);
			let backwards = if rng.gen_bool(0.5) {
				-1.0
			} else {
				1.0
			};
			let speed = rng.gen_range(0.2,2.0);
			let speed = if(vertical) {
				graphics::Point2::new(0.0, speed*backwards)
			} else {
				graphics::Point2::new(speed * backwards, 0.0)
			};
			let point = graphics::Point2::new(rng.gen_range(0., screen_res.0.w), rng.gen_range(0., screen_res.0.h));
			let scale = rng.gen_range(0.1, 1.0);
			let scalePoint = graphics::Point2::new(scale, scale);
			let texture = rng.choose(&imgs.0).unwrap();
			let alpha = rng.gen_range(0.8, 1.0);
			updater.insert(
				symbol,
				Position{point}
			);
			updater.insert(
				symbol,
				Scale(scalePoint),
			);
			updater.insert(
				symbol,
				Speed(speed)
			);
			updater.insert(
				symbol,
				Texture{texture: texture.clone()}
			);
			updater.insert(
				symbol,
				Alpha(alpha)
			);
		}
	}
}


impl MainState {
	fn new(ctx: &mut Context) -> GameResult<MainState> {
		// The ttf file will be in your resources directory. Later, we
		// will mount that directory so we can omit it in the path here.
		//let font = graphics::Font::new(ctx, "/DejaVuSerif.ttf", 12)?;
		
		let mut rng = rand::thread_rng();
		
		let mut world = World::new();
		world.add_resource(DeltaTime(0));
		let mut dispatcher = DispatcherBuilder::new()
			.with(Mover, "mover", &[])
 			.with(Reaper, "reaper", &[])
 			.with(Birther, "birther", &[])
			.build();
		dispatcher.setup(&mut world.res);
		
		world.register::<Scale>();
		world.register::<Texture>();
		world.register::<Alpha>();
		
		let mut s = MainState {
			world,
			dispatcher,
			frames: 0,
			texture_map: HashMap::new(),
		};
		
		
		// Generate actors
		
		let screen_rect = graphics::get_screen_coordinates(ctx);
		let fleet_margin = 10.;
		let ship_spacing = 5.;
	
		let mut img_vector = vec![];
		for img in ctx.filesystem.read_dir("/")? {
			img_vector.push(s.get_image(ctx, img.to_str().unwrap().to_string()).unwrap());
		};
		
		s.world.add_resource(Imgs(img_vector));
		
		
		
		Ok(s)
	}
	
	fn get_image(&mut self, ctx: &mut Context, path: String) -> GameResult<graphics::Image> {
		match self.texture_map.entry(path.clone()) {
			Entry::Occupied(e) => return Ok(e.get().clone()),
			Entry::Vacant(e) => {
				let image = graphics::Image::new(ctx, path)?;
				e.insert(image.clone());
				return Ok(image);
			}
		}
	}
}

// Then we implement the `ggez:event::EventHandler` trait on it, which
// requires callbacks for updating and drawing the game state each frame.
//
// The `EventHandler` trait also contains callbacks for event handling
// that you can override if you wish, but the defaults are fine.
impl event::EventHandler for MainState {
	fn update(&mut self, ctx: &mut Context) -> GameResult<()> {
		let delta = timer::get_delta(ctx);
		
		{
			let mut delta_res = self.world.write_resource::<DeltaTime>();
			*delta_res = DeltaTime(delta.as_secs() as u64 +delta.subsec_nanos() as u64);
		}
		
		let screen_rect = graphics::get_screen_coordinates(ctx);
		{
			let mut screen_res = self.world.write_resource::<ScreenResolution>();
			*screen_res = ScreenResolution(screen_rect)
		}
		
		self.dispatcher.dispatch(&mut self.world.res);
		self.world.maintain();
		
		Ok(())
	}

	fn draw(&mut self, ctx: &mut Context) -> GameResult<()> {
		graphics::clear(ctx);
		
		use specs::Join;
		
		let (positions, texture, scale, alpha) = (
			self.world.read_storage::<Position>(),
			self.world.read_storage::<Texture>(),
			self.world.read_storage::<Scale>(),
			self.world.read_storage::<Alpha>(),
		);

 		
		
		graphics::set_color(ctx, graphics::WHITE)?;
		let screen = graphics::get_screen_coordinates(ctx);
		graphics::rectangle(ctx, Fill, screen)?;
		for (position, texture, scale, alpha) in (&positions, &texture, &scale, &alpha).join() {
			graphics::set_color(ctx, graphics::Color::new(1.0, 1.0, 1.0, alpha.0 as f32))?;
			graphics::draw_ex(ctx, &texture.texture, graphics::DrawParam{ dest: position.point, scale: scale.0, .. Default::default()})?;
// 			println!("pos: {}", position.point)
		}
		
		
		graphics::present(ctx);

		self.frames += 1;
		if (self.frames % 100) == 0 {
		println!("FPS: {}", ggez::timer::get_fps(ctx));
		}

		Ok(())
	}
}

// Now our main function, which does three things:
//
// * First, create a new `ggez::conf::Conf`
// object which contains configuration info on things such
// as screen resolution and window title.
// * Second, create a `ggez::game::Game` object which will
// do the work of creating our MainState and running our game.
// * Then, just call `game.run()` which runs the `Game` mainloop.
pub fn main() {
	let mut c = conf::Conf::new();
	c.window_mode=WindowMode {
		width: 1920,
		height: 1080,
		borderless: false,
		fullscreen_type: FullscreenType::Off,
		vsync: true,
		min_width: 0,
		max_width: 0,
		min_height: 0,
		max_height: 0,
		};
	let ctx = &mut Context::load_from_conf("helloworld", "ggez", c).unwrap();

	// We add the CARGO_MANIFEST_DIR/resources to the filesystem's path
	// so that ggez will look in our cargo project directory for files.
	if let Ok(manifest_dir) = env::var("CARGO_MANIFEST_DIR") {
		let mut path = path::PathBuf::from(manifest_dir);
		path.push("resources");
		ctx.filesystem.mount(&path, true);
	}

	let state = &mut MainState::new(ctx).unwrap();
	if let Err(e) = event::run(ctx, state) {
		println!("Error encountered: {}", e);
	} else {
		println!("Game exited cleanly.");
	}
}
